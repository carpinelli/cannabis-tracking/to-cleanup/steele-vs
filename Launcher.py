#! /usr/bin/env python3

"""A Tk GUI that launches Steele-VS scripts."""
from pathlib import Path
import tkinter as tk
from tkinter import filedialog


"""Issues
from tkinter.filedialog import askopenfilename
add function from gen_harvest_csv:

def get_filename(Prompt: str) -> str:
    "#"#"Opens a Tk filedialog window for a user to select a file, that
    file's name is returned as a string."#"#"
    root = Tk()  # get a main window so we can destroy it later
    root.withdraw()  # hide main window so only the filedialog is visible

    # Workaround to hide hidden files
    # Undocumented usage, hence the outer try/catch block and bare
    # exception
    try:
        # Call a dummy dialog using a non-existant option
        # This will not give a file dialog, but will throw a TclError
        # This will create the parent namespace necessary to call
        # set commands below
        try:
            root.tk.call("tk_getOpenFile", "-foobarbaz")
        except Exception:  # _tkinter.TclError
            pass
        # Set inner/magic variables
        root.tk.call("set", "::tk::dialog::file::showHiddenBtn", '1')
        root.tk.call("set", "::tk::dialog::file::showHiddenVar", '0')
    except Exception:
        logger.warn(traceback.format_exc())
        logger.warn("An error occurred, hidden files will now be shown...")

    filename = askopenfilename(title=Prompt, filetypes=Filetypes)
    # get filename

    root.destroy()  # destroy main window created earlier

    return filename
"""


def output_directory():
    """Returns the user's desktop folder as a Path object."""
    return Path.home() / "Desktop"


def change_directory(button: tk.Button,
                     label: tk.Label) -> None:
    """Changes directory using tkinter's filedialog.askdirectory,
    and updates the label to the chosen file."""
    Prompt = "Select the directory for the output files..."
    Empty = (None, "", [], (), {})

    outputDir = label["text"]
    new_outputDir = filedialog.askdirectory(title=Prompt,
                                            initialdir=outputDir)

    # if cancel button is pressed...
    if new_outputDir not in Empty:
        label.config(text=new_outputDir)

    return


class Launcher(tk.Frame):
    """A Tk application window for launching scripts."""
    def __init__(self, master: tk.Tk = None):
        """Constructor."""
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        """Creates and loads widgets for main launcher."""
        # Frames
        self.scriptFrm = tk.Frame(self, bd=16)  # script frame
        self.optionFrm = tk.Frame(self, bd=4)  # option frame
        self.scriptFrm.pack(side=tk.TOP)
        self.optionFrm.pack(side=tk.BOTTOM)

        # Buttons and Labels #

        # Gen Harvest CSV
        self.harvestBttn = tk.Button(self.scriptFrm, text="Gen Harvest CSV",
                                     command=self.gen_harvest_csv)
        self.harvestBttn.pack(side=tk.TOP)
        # self.harvestBttn["text"] = "Gen Harvest CSV"
        # self.harvestBttn["command"] = self.gen_harvest_csv

        # Inventory / Heat Map
        self.inventoryBttn = tk.Button(self.scriptFrm, text="Gen Inventory",
                                       command=self.gen_inventory)
        self.inventoryBttn.pack(side=tk.BOTTOM)

        # Download Metrc Report
        self.reportBttn = tk.Button(self.scriptFrm,
                                    text="Download Inventory "
                                    "Report",
                                    command=self.download_inventory)
        self.reportBttn.pack(side=tk.BOTTOM)

        # Change output directory button
        self.choose_directoryLbl = tk.Label(self.optionFrm,
                                            text="Change the output folder:")
        self.output_directoryLbl = tk.Label(self.optionFrm,
                                            text=output_directory(),
                                            relief=tk.SUNKEN,
                                            bg="white")
        self.choose_directoryBttn = tk.Button(self.optionFrm, text="...",
                                              command=lambda:
                                              change_directory(
                                                  self.choose_directoryBttn,
                                                  self.output_directoryLbl))
        self.choose_directoryLbl.pack(side=tk.TOP)
        self.output_directoryLbl.pack(side=tk.LEFT)
        self.choose_directoryBttn.pack(side=tk.LEFT)

    def gen_harvest_csv(self):
        """Import and call the gen_harvest_csv module."""
        # from scripts import gen_harvest_csv
        # gen_harvest_csv.main(self.output_directoryLbl["text"]))
        pass

    def download_inventory(self):
        """Import and call the download_inventory module."""
        # from scripts import download_inventory
        # download_inventory.main(self.output_directoryLbl["text"]))
        pass

    def gen_inventory(self):
        """Import and call the gen_inventory module."""
        # from scripts import gen_inventory
        # gen_inventory.main(self.output_directoryLbl["text"]))
        pass


def main():
    # Setup main window
    root = tk.Tk()
    launcher = Launcher(master=root)
    launcher.master.title("Steele-VS Launcher")
    launcher.master.minsize(400, 200)
    launcher.master.maxsize(1000, 400)

    # Run the window
    launcher.mainloop()

    return


if __name__ == "__main__":
    main()
